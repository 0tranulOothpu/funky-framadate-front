import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShortcutsHelpComponent } from './shortcuts-help.component';

describe('ShortcutsHelpComponent', () => {
	let component: ShortcutsHelpComponent;
	let fixture: ComponentFixture<ShortcutsHelpComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ShortcutsHelpComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ShortcutsHelpComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
