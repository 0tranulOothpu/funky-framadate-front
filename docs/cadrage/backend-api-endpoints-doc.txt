# temporary documentation of backend API endpoints

 ------------------------------------------ ---------- -------- ------ ------------------------------------------------
  Name                                       Method     Scheme   Host   Path
 ------------------------------------------ ---------- -------- ------ ------------------------------------------------
  _twig_error_test                           ANY        ANY      ANY    /_error/{code}.{_format}
  admin_homepage_get_default                 GET        ANY      ANY    /admin/
  admin_homepage_clean_expired_polls         GET        ANY      ANY    /admin/polls/clean/{token}
  admin_homepage_migrate_framadate           GET        ANY      ANY    /admin/polls/migrate
  api_get_poll_comment                       GET        ANY      ANY    /api/v1/poll/{id}/comments
  api_new_comment                            POST       ANY      ANY    /api/v1/poll/{id}/comment
  api_poll_comments_delete                   DELETE     ANY      ANY    /api/v1/poll/{id}/comments
  api_page_home                              GET        ANY      ANY    /page/
  user_homepageget_default                   GET        ANY      ANY    /user/
  user_homepage_polls_send_by_email          GET        ANY      ANY    /user/{email}/polls/send-by-email
  poll_index                                 GET        ANY      ANY    /poll/
  poll_new                                   GET|POST   ANY      ANY    /poll/new
  poll_show                                  GET        ANY      ANY    /poll/{id}
  poll_edit                                  GET|POST   ANY      ANY    /poll/{id}/edit
  poll_delete                                DELETE     ANY      ANY    /poll/{id}
  api_new_vote_stack                         POST       ANY      ANY    /api/v1/poll/{id}/answer
  api_update_vote_stack                      PATCH      ANY      ANY    /api/v1/vote-stack/{id}/token/{modifierToken}
  api_poll_votes_delete                      DELETE     ANY      ANY    /api/v1/poll/{id}/votes/{accessToken}
  api_get_all_polls                          GET        ANY      ANY    /api/v1/poll/
  api_get_poll                               GET        ANY      ANY    /api/v1/poll/{id}
  api_update_poll                            PUT        ANY      ANY    /api/v1/poll/{id}/{token}
  api_new_poll                               POST       ANY      ANY    /api/v1/poll/
  api_test-mail-poll                         GET        ANY      ANY    /api/v1/poll/mail/test-mail-poll/{emailChoice}
  api_poll_delete                            DELETE     ANY      ANY    /api/v1/poll/{id}
  api_check_slug_is_unique                   GET        ANY      ANY    /api/v1/poll/slug/{slug}
  api_get_admin_config                       GET        ANY      ANY    /api/v1/poll/admin/{token}
  overblog_graphql_endpoint                  ANY        ANY      ANY    /api/graphql/
  overblog_graphql_batch_endpoint            ANY        ANY      ANY    /api/graphql/batch
  overblog_graphql_multiple_endpoint         ANY        ANY      ANY    /api/graphql/graphql/{schemaName}
  overblog_graphql_batch_multiple_endpoint   ANY        ANY      ANY    /api/graphql/graphql/{schemaName}/batch
  app.swagger                                GET        ANY      ANY    /api/doc.json
 ------------------------------------------ ---------- -------- ------ ------------------------------------------------


/**
 * WANTED CHANGES (seraf)
 * -------------------------- -------- -------- ------ ------------------------------------------------
 Name                           Method   Scheme   Host   Path
 -------------------------- -------- -------- ------ ------------------------------------------------
 api_get_poll_comment           GET      ANY      ANY    /polls/{id}/comment
 api_delete_poll_comments       DELETE   ANY      ANY    /polls/{id}/comment
 api_user_polls_send_by_email   GET      ANY      ANY    /users/{email}/polls/send-by-email
 api_get_user_polls             GET      ANY      ANY    /users/{email}/polls
 api_get_poll_slug              GET      ANY      ANY    /polls/slug/{id}/{token}
 api_clean_expired_polls        GET      ANY      ANY    /admin/clean-polls/{token}

 api_test-mail-polls            GET      ANY      ANY    /polls/mail/test-mail-polls/{emailChoice}
 api_update_vote_stack          PATCH    ANY      ANY    /polls/{slug}/answers/{pseudo}/token/{modifierToken}

 -------------------------- -------- -------- ------ ------------------------------------------------
 */
