import { Choice, ChoiceGroup, ChoiceText } from './choice.model';
import { Comment } from './comment.model';
import { Owner } from './owner.model';
import { DateChoice, TimeSlices } from './dateChoice.model';
import { defaultTimeOfDay } from '../../../../mocks/old-stuff/config/defaultConfigs';

export class Poll {
	public admin_key: string;
	public allow_comments?: boolean = true;
	public allowComments?: boolean = true;
	public allowed_answers = ['yes', 'maybe', 'no'];
	public allowSeveralHours?: boolean;
	public archiveNumberOfDays?: number;
	public areResultsPublic?: boolean = true;
	public choices: Choice[] = [];
	public choicesText: ChoiceText[] = [];
	public choices_grouped: ChoiceGroup[] = [];
	public comments: Comment[] = [];
	public creation_date?: string = new Date().toISOString();
	public creatorEmail?: string;
	public creatorPseudo?: string;
	public dateChoices: DateChoice[] = [];
	public default_expiracy_days_from_now = 60;
	public description?: string;
	public expiracy_date?: string;
	public has_several_hours?: boolean = false;
	public hasSeveralHours?: boolean = false;
	public id = 0;
	public is_archived?: boolean;
	public isMaybeAnswerAvailable?: boolean = true;
	public isOwnerNotifiedByEmailOnNewComment?: boolean = true;
	public isOwnerNotifiedByEmailOnNewVote?: boolean = true;
	public is_zero_knowledge?: boolean = false;
	public kind = 'date';
	public hideResults = false;
	public max_count_of_answers?: number = 150;
	public maxCountOfAnswers?: number = 150;
	public max_score?: number;
	public modification_policy = 'everybody';
	public password?: string;
	public stacks = [];
	public timeSlices: TimeSlices[] = Object.create(defaultTimeOfDay); // ranges of time expressed as strings
	public votes = [];
	public votes_allowed: boolean = true;

	constructor(public owner: Owner = new Owner(), public title = 'mon titre', public custom_url: string = '') {}
}
